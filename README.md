# README #

### How do I get set up? ###

* clone this repo
* go to the directory with the `pom.xml`
* from a console window type `mvn jetty:run`
* wait for `[INFO] Started Jetty Server`
* launch your browser at `localhost:8080`
* read scenario instructions
* do a pull request with your code when you are done
