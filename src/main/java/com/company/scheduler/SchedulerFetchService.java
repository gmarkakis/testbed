package com.company.scheduler;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;
import static org.quartz.JobBuilder.newJob;
import com.company.quartz.RetrieveAndStoreJob;

/**
 * Scheduler for retrieval/storage job
 * 
 * @author Georgios Markakis
 */
@Service
public class SchedulerFetchService {

	/**
	 * Injects SchedulerFactoryBean from Spring context
	 */
	@Autowired
	SchedulerFactoryBean schedulerFactoryBean;

	/**
	 * Registers and triggers a Scheduler Job
	 * 
	 * @throws SchedulerException
	 */
	public void startScheduling() throws SchedulerException {
		JobKey.jobKey("epant_get_news");
		JobDetail job = newJob(RetrieveAndStoreJob.class).withIdentity("epant_get_news").storeDurably().build();
		JobKey jobKey = job.getKey();
		schedulerFactoryBean.getScheduler().addJob(job, true);
		schedulerFactoryBean.getScheduler().triggerJob(jobKey);
		schedulerFactoryBean.getScheduler().start();
	}
}
