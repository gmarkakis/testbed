package com.company.scheduler;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import com.company.rest.RestFetchService;
import static org.zkoss.zk.ui.util.Clients.showNotification;
import org.quartz.SchedulerException;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

/**
 * Gui interaction
 * 
 * @author Georgios Markakis
 */
@VariableResolver(DelegatingVariableResolver.class)
public class BoardViewModel {

	/**
	 * The Scheduler service
	 */
	@WireVariable
	SchedulerFetchService schedulerFetchService;

	/**
	 * The Rest Service
	 */
	@WireVariable
	RestFetchService restFetchService;

	/**
	 * 
	 */
	@Command
	public void startGetNewsJob() {
		// launch job epant_get_news on demand and return
		try {
			schedulerFetchService.startScheduling();
			showNotification("Job executed successfully", true);
		} catch (SchedulerException e1) {
			showNotification("Something went wrong", true);
			e1.printStackTrace();
		}

	}
}
