package com.company.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.company.config.Application;
import com.company.dao.DocumentRepositoryService;
import com.company.model.Documents;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Rest Controller that exposes the received data
 * 
 * @author Georgios Markakis
 */
@RestController
public class AppRestController {

	/**
	 * LOGGER
	 */
	Logger logger = LoggerFactory.getLogger(Application.class);

	@Autowired
	private DocumentRepositoryService docService;

	@RequestMapping("/GetMyNews")
	public Documents index() {
		logger.debug("Calling Rest Service");
		return docService.fetchStoredDocuments();
	}

}
