package com.company.dao;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.company.config.Application;
import com.company.model.Decision;
import com.company.model.Documents;
import com.company.model.Info;
import com.company.model.DocumentRepository;

/**
 * Service that exposes the DocumentRepository 
 * from Spring context
 * 
 * @author Georgios Markakis
 */
@Service
@Transactional
public class DocumentRepositoryService {
	
	/**
	 * LOGGER
	 */
	Logger logger = LoggerFactory.getLogger(Application.class);
	
	
	/**
	 * Data Repository
	 */
	@Autowired
    private	DocumentRepository repository;
	
	
	/**
	 * Fetches all stored documents in the Database
	 * 
	 * @return
	 */
	public Documents fetchStoredDocuments(){
		logger.debug("Storing " );
	    List<Decision> decisions = repository.findAll();
		logger.debug("Retrieving " + decisions.size() + "documents");
	    Documents docs = new Documents();
	    docs.setDecisions(decisions);
	    Info info = new Info();
	    info.setTotal(decisions.size());
		docs.setInfo(info );
		return docs;
	}
	
	
	/**
	 * Stores JSON serialized data objects in the database
	 * 
	 * @param docs
	 */
	public void storeDocuments(Documents docs){
		List<Decision> decisions = docs.getDecisions();
		logger.debug("Storing " + decisions.size() + "documents");
		repository.save(decisions);
	}

}
