package com.company.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.company.config.Application;
import com.company.dao.DocumentRepositoryService;
import com.company.model.Documents;
import com.company.rest.RestFetchService;

/**
 * Spring Quartz job ran by the scheduler
 * 
 * @author Georgios Markakis
 */
public class RetrieveAndStoreJob implements Job {

	/**
	 * LOGGER
	 */
	private Logger logger = LoggerFactory.getLogger(Application.class);

	/**
	 * The external call Rest Client
	 */
	@Autowired
	private RestFetchService restFetchService;

	/**
	 * The data storage service
	 */
	@Autowired
	private DocumentRepositoryService docService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		logger.debug("Job Initiated");
		Documents docs = restFetchService.fetchDocs();
		docService.storeDocuments(docs);
	}

}
