package com.company.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.company.model.Documents;

/**
 * Fetches the data from Diavgeia "https://test3.diavgeia.gov.gr
 * 
 * @author Georgios Markakis
 */
@Service
public class RestFetchService {

	private static final String SERVICEURL = "https://test3.diavgeia.gov.gr/luminapi/opendata/"
			+ "search.json?orgUId=epant&to_date=2015-11-01&size=10";
	/**
	 * Injects the Rest Template from Spring context
	 */
	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Fetches and serialized JSON to Jackson objects
	 * 
	 * @return
	 */
	public Documents fetchDocs() {
		Documents docs = restTemplate.getForObject(SERVICEURL, Documents.class);
		return docs;

	}
}
