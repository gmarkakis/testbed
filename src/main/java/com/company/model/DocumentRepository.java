package com.company.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Base interface for Document Repository definition
 * 
 * @author Georgios Markakis
 *
 */
@Repository
public interface DocumentRepository extends JpaRepository<Decision, Long> {

}
