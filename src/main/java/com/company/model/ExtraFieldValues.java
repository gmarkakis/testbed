package com.company.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * JPA/Jackson Entity (ExtraFieldValues) the most troublesome and densely
 * populated one
 * 
 * @author Georgios Markakis
 * 
 */
// TODO:Explicitly excluded fields in order to include them later
@JsonIgnoreProperties(ignoreUnknown = true, value = { "person", "awardAmount", "assignmentType", "cpv",
		"textRelatedADA", "eidosPraxis", "positionSalary", "position", "employerOrg" })
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "org", "sponsor", "relatedAnalipsiYpoxreosis", "relatedDecisions", "documentType", "financialYear",
		"budgettype", "entryNumber", "partialead", "recalledExpenseDecision", "amountWithVAT", "amountWithKae",
		"relatedPartialADA" })
@Entity
public class ExtraFieldValues implements Serializable {

	/**
	 *  	 
	 */
	private static final long serialVersionUID = 1L;

	// Not good but it will do for now
	@Id
	@GeneratedValue
	private Integer id;

	@JsonProperty("org")
	@ManyToOne(cascade = CascadeType.ALL)
	private Org org;

	@JsonProperty("sponsor")
	@Column(name = "sponsor")
	@Embedded
	private List<Sponsor> sponsor = null;

	@JsonProperty("relatedAnalipsiYpoxreosis")
	@Embedded
	private List<RelatedAnalipsiYpoxreosi> relatedAnalipsiYpoxreosis = null;

	@JsonProperty("relatedDecisions")
	@Column(name = "relatedDecisions")
	private String relatedDecisions;

	@JsonProperty("documentType")
	@Column(name = "documentType")
	private String documentType;

	@JsonProperty("financialYear")
	@Column(name = "financialYear")
	private Integer financialYear;

	@JsonProperty("budgettype")
	@Column(name = "budgettype")
	private String budgettype;

	@JsonProperty("entryNumber")
	@Column(name = "entryNumber")
	private String entryNumber;

	@JsonProperty("partialead")
	@Column(name = "partialead")
	private Boolean partialead;

	@JsonProperty("recalledExpenseDecision")
	@Column(name = "recalledExpenseDecision")
	private Boolean recalledExpenseDecision;

	@JsonProperty("amountWithVAT")
	@Column(name = "amountWithVAT")
	@Embedded
	private AmountWithVAT amountWithVAT;

	@JsonProperty("amountWithKae")
	@Column(name = "amountWithKae")
	@Embedded
	private List<AmountWithKae> amountWithKae;

	@JsonProperty("relatedPartialADA")
	@Column(name = "relatedPartialADA")
	private String relatedPartialADA;

	@JsonIgnore
	@Column(name = "additionalProperties")
	@Embedded
	private Map<String, String> additionalProperties;

	@JsonProperty("org")
	public Org getOrg() {
		return org;
	}

	@JsonProperty("org")
	public void setOrg(Org org) {
		this.org = org;
	}

	@JsonProperty("sponsor")
	public List<Sponsor> getSponsor() {
		return sponsor;
	}

	@JsonProperty("sponsor")
	public void setSponsor(List<Sponsor> sponsor) {
		this.sponsor = sponsor;
	}

	@JsonProperty("relatedAnalipsiYpoxreosis")
	public List<RelatedAnalipsiYpoxreosi> getRelatedAnalipsiYpoxreosis() {
		return relatedAnalipsiYpoxreosis;
	}

	@JsonProperty("relatedAnalipsiYpoxreosis")
	public void setRelatedAnalipsiYpoxreosis(List<RelatedAnalipsiYpoxreosi> relatedAnalipsiYpoxreosis) {
		this.relatedAnalipsiYpoxreosis = relatedAnalipsiYpoxreosis;
	}

	@JsonProperty("relatedDecisions")
	public Object getRelatedDecisions() {
		return relatedDecisions;
	}

	@JsonProperty("relatedDecisions")
	public void setRelatedDecisions(String relatedDecisions) {
		this.relatedDecisions = relatedDecisions;
	}

	@JsonProperty("documentType")
	public String getDocumentType() {
		return documentType;
	}

	@JsonProperty("documentType")
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	@JsonProperty("financialYear")
	public Integer getFinancialYear() {
		return financialYear;
	}

	@JsonProperty("financialYear")
	public void setFinancialYear(Integer financialYear) {
		this.financialYear = financialYear;
	}

	@JsonProperty("budgettype")
	public String getBudgettype() {
		return budgettype;
	}

	@JsonProperty("budgettype")
	public void setBudgettype(String budgettype) {
		this.budgettype = budgettype;
	}

	@JsonProperty("entryNumber")
	public String getEntryNumber() {
		return entryNumber;
	}

	@JsonProperty("entryNumber")
	public void setEntryNumber(String entryNumber) {
		this.entryNumber = entryNumber;
	}

	@JsonProperty("partialead")
	public Boolean getPartialead() {
		return partialead;
	}

	@JsonProperty("partialead")
	public void setPartialead(Boolean partialead) {
		this.partialead = partialead;
	}

	@JsonProperty("recalledExpenseDecision")
	public Boolean getRecalledExpenseDecision() {
		return recalledExpenseDecision;
	}

	@JsonProperty("recalledExpenseDecision")
	public void setRecalledExpenseDecision(Boolean recalledExpenseDecision) {
		this.recalledExpenseDecision = recalledExpenseDecision;
	}

	@JsonProperty("amountWithVAT")
	public AmountWithVAT getAmountWithVAT() {
		return amountWithVAT;
	}

	@JsonProperty("amountWithVAT")
	public void setAmountWithVAT(AmountWithVAT amountWithVAT) {
		this.amountWithVAT = amountWithVAT;
	}

	@JsonProperty("amountWithKae")
	public List<AmountWithKae> getAmountWithKae() {
		return amountWithKae;
	}

	@JsonProperty("amountWithKae")
	public void setAmountWithKae(List<AmountWithKae> amountWithKae) {
		this.amountWithKae = amountWithKae;
	}

	@JsonProperty("relatedPartialADA")
	public Object getRelatedPartialADA() {
		return relatedPartialADA;
	}

	@JsonProperty("relatedPartialADA")
	public void setRelatedPartialADA(String relatedPartialADA) {
		this.relatedPartialADA = relatedPartialADA;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, String> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, String value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(org).append(sponsor).append(relatedAnalipsiYpoxreosis)
				.append(relatedDecisions).append(documentType).append(financialYear).append(budgettype)
				.append(entryNumber).append(partialead).append(recalledExpenseDecision).append(amountWithVAT)
				.append(amountWithKae).append(relatedPartialADA).toHashCode();
		// .append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ExtraFieldValues) == false) {
			return false;
		}
		ExtraFieldValues rhs = ((ExtraFieldValues) other);
		return new EqualsBuilder().append(org, rhs.org).append(sponsor, rhs.sponsor)
				.append(relatedAnalipsiYpoxreosis, rhs.relatedAnalipsiYpoxreosis)
				.append(relatedDecisions, rhs.relatedDecisions).append(documentType, rhs.documentType)
				.append(financialYear, rhs.financialYear).append(budgettype, rhs.budgettype)
				.append(entryNumber, rhs.entryNumber).append(partialead, rhs.partialead)
				.append(recalledExpenseDecision, rhs.recalledExpenseDecision).append(amountWithVAT, rhs.amountWithVAT)
				.append(amountWithKae, rhs.amountWithKae).append(relatedPartialADA, rhs.relatedPartialADA).isEquals();
		// .append(additionalProperties, rhs.additionalProperties).isEquals();
	}

}