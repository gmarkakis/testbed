package com.company.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Embeddable;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * JPA/Jackson Entity (AmountWithKae)
 * 
 * @author Georgios Markakis
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "amountWithVAT", "kae", "kaeBudgetRemainder", "kaeCreditRemainder", "sponsorAFMName" })
@Embeddable
public class AmountWithKae implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("amountWithVAT")
	private Double amountWithVAT;

	@JsonProperty("kae")
	private String kae;

	@JsonProperty("kaeBudgetRemainder")
	private Double kaeBudgetRemainder;

	@JsonProperty("kaeCreditRemainder")
	private Double kaeCreditRemainder;

	@JsonIgnore
	private Map<String, String> additionalProperties = new HashMap<String, String>();

	@JsonProperty("amountWithVAT")
	public Double getAmountWithVAT() {
		return amountWithVAT;
	}

	@JsonProperty("amountWithVAT")
	public void setAmountWithVAT(Double amountWithVAT) {
		this.amountWithVAT = amountWithVAT;
	}

	@JsonProperty("kae")
	public String getKae() {
		return kae;
	}

	@JsonProperty("kae")
	public void setKae(String kae) {
		this.kae = kae;
	}

	@JsonProperty("kaeBudgetRemainder")
	public Double getKaeBudgetRemainder() {
		return kaeBudgetRemainder;
	}

	@JsonProperty("kaeBudgetRemainder")
	public void setKaeBudgetRemainder(Double kaeBudgetRemainder) {
		this.kaeBudgetRemainder = kaeBudgetRemainder;
	}

	@JsonProperty("kaeCreditRemainder")
	public Double getKaeCreditRemainder() {
		return kaeCreditRemainder;
	}

	@JsonProperty("kaeCreditRemainder")
	public void setKaeCreditRemainder(Double kaeCreditRemainder) {
		this.kaeCreditRemainder = kaeCreditRemainder;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, String> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, String value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(amountWithVAT).append(kae).append(kaeBudgetRemainder)
				.append(kaeCreditRemainder).append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof AmountWithKae) == false) {
			return false;
		}
		AmountWithKae rhs = ((AmountWithKae) other);
		return new EqualsBuilder().append(amountWithVAT, rhs.amountWithVAT).append(kae, rhs.kae)
				.append(kaeBudgetRemainder, rhs.kaeBudgetRemainder).append(kaeCreditRemainder, rhs.kaeCreditRemainder)
				.append(additionalProperties, rhs.additionalProperties).isEquals();
	}

}