package com.company.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * JPA/Jackson Entity (SponsorAFMName)
 * 
 * @author Georgios Markakis
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "afm", "afmType", "afmCountry", "name", "noVATOrg" })
@Entity
public class SponsorAFMName implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("afm")
	@Id
	@Column(name = "afm")
	private String afm;

	@JsonProperty("afmType")
	@Column(name = "afmType")
	private String afmType;

	@JsonProperty("afmCountry")
	@Column(name = "afmCountry")
	private String afmCountry;

	@JsonProperty("name")
	@Column(name = "name")
	private String name;

	@JsonProperty("noVATOrg")
	@Column(name = "noVATOrg")
	private String noVATOrg;

	@JsonIgnore
	@Embedded
	private Map<String, String> additionalProperties = new HashMap<String, String>();

	@JsonProperty("afm")
	public String getAfm() {
		return afm;
	}

	@JsonProperty("afm")
	public void setAfm(String afm) {
		this.afm = afm;
	}

	@JsonProperty("afmType")
	public String getAfmType() {
		return afmType;
	}

	@JsonProperty("afmType")
	public void setAfmType(String afmType) {
		this.afmType = afmType;
	}

	@JsonProperty("afmCountry")
	public String getAfmCountry() {
		return afmCountry;
	}

	@JsonProperty("afmCountry")
	public void setAfmCountry(String afmCountry) {
		this.afmCountry = afmCountry;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("noVATOrg")
	public String getNoVATOrg() {
		return noVATOrg;
	}

	@JsonProperty("noVATOrg")
	public void setNoVATOrg(String noVATOrg) {
		this.noVATOrg = noVATOrg;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, String> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, String value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(afm).append(afmType).append(afmCountry).append(name).append(noVATOrg)
				.append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof SponsorAFMName) == false) {
			return false;
		}
		SponsorAFMName rhs = ((SponsorAFMName) other);
		return new EqualsBuilder().append(afm, rhs.afm).append(afmType, rhs.afmType).append(afmCountry, rhs.afmCountry)
				.append(name, rhs.name).append(noVATOrg, rhs.noVATOrg)
				.append(additionalProperties, rhs.additionalProperties).isEquals();
	}

}