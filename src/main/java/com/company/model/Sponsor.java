package com.company.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * JPA/Jackson Entity (Sponsor)
 * 
 * @author Georgios Markakis
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "sponsorAFMName", "expenseAmount", "cpv", "kae" })
@Entity
public class Sponsor implements Serializable {

	private static final long serialVersionUID = 1L;

	// Not good but it will do for now
	@Id
	@GeneratedValue
	private int id;

	@JsonProperty("sponsorAFMName")
	@ManyToOne
	private SponsorAFMName sponsorAFMName;

	@JsonProperty("expenseAmount")
	@Embedded
	private ExpenseAmount expenseAmount;

	@JsonProperty("cpv")
	@Column(name = "cpv")
	private String cpv;

	@JsonProperty("kae")
	@Column(name = "kae")
	private String kae;

	@JsonIgnore
	@Column(name = "additionalProperties")
	@Embedded
	private Map<String, String> additionalProperties = new HashMap<String, String>();

	@JsonProperty("sponsorAFMName")
	public SponsorAFMName getSponsorAFMName() {
		return sponsorAFMName;
	}

	@JsonProperty("sponsorAFMName")
	public void setSponsorAFMName(SponsorAFMName sponsorAFMName) {
		this.sponsorAFMName = sponsorAFMName;
	}

	@JsonProperty("expenseAmount")
	public ExpenseAmount getExpenseAmount() {
		return expenseAmount;
	}

	@JsonProperty("expenseAmount")
	public void setExpenseAmount(ExpenseAmount expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	@JsonProperty("cpv")
	public String getCpv() {
		return cpv;
	}

	@JsonProperty("cpv")
	public void setCpv(String cpv) {
		this.cpv = cpv;
	}

	@JsonProperty("kae")
	public String getKae() {
		return kae;
	}

	@JsonProperty("kae")
	public void setKae(String kae) {
		this.kae = kae;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, String> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, String value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(sponsorAFMName).append(expenseAmount).append(cpv).append(kae)
				.append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Sponsor) == false) {
			return false;
		}
		Sponsor rhs = ((Sponsor) other);
		return new EqualsBuilder().append(sponsorAFMName, rhs.sponsorAFMName).append(expenseAmount, rhs.expenseAmount)
				.append(cpv, rhs.cpv).append(kae, rhs.kae).append(additionalProperties, rhs.additionalProperties)
				.isEquals();
	}

}