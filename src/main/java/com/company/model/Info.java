package com.company.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Jackson Entity (Info)
 * 
 * @author Georgios Markakis
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"query",
"page",
"size",
"actualSize",
"total",
"order"
})
public class Info implements Serializable{

/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
@JsonProperty("query")
private String query;
@JsonProperty("page")
private Integer page;
@JsonProperty("size")
private Integer size;
@JsonProperty("actualSize")
private Integer actualSize;
@JsonProperty("total")
private Integer total;
@JsonProperty("order")
private String order;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("query")
public String getQuery() {
return query;
}

@JsonProperty("query")
public void setQuery(String query) {
this.query = query;
}

@JsonProperty("page")
public Integer getPage() {
return page;
}

@JsonProperty("page")
public void setPage(Integer page) {
this.page = page;
}

@JsonProperty("size")
public Integer getSize() {
return size;
}

@JsonProperty("size")
public void setSize(Integer size) {
this.size = size;
}

@JsonProperty("actualSize")
public Integer getActualSize() {
return actualSize;
}

@JsonProperty("actualSize")
public void setActualSize(Integer actualSize) {
this.actualSize = actualSize;
}

@JsonProperty("total")
public Integer getTotal() {
return total;
}

@JsonProperty("total")
public void setTotal(Integer total) {
this.total = total;
}

@JsonProperty("order")
public String getOrder() {
return order;
}

@JsonProperty("order")
public void setOrder(String order) {
this.order = order;
}

@Override
public String toString() {
return ToStringBuilder.reflectionToString(this);
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(query).append(page).append(size).append(actualSize).append(total).append(order).append(additionalProperties).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof Info) == false) {
return false;
}
Info rhs = ((Info) other);
return new EqualsBuilder().append(query, rhs.query).append(page, rhs.page).append(size, rhs.size).append(actualSize, rhs.actualSize).append(total, rhs.total).append(order, rhs.order).append(additionalProperties, rhs.additionalProperties).isEquals();
}

}