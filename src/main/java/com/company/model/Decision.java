package com.company.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * JPA/Jackson Entity (Decision)
 * 
 * @author Georgios Markakis
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "protocolNumber", "subject", "issueDate", "organizationId", "decisionTypeId",
		"thematicCategoryIds", "extraFieldValues", "privateData", "ada", "publishTimestamp", "submissionTimestamp",
		"versionId", "status", "url", "documentUrl", "documentChecksum", "attachments", "warnings",
		"correctedVersionId" })
@Entity
public class Decision implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("protocolNumber")
	@Column(name = "protocolNumber")
	private String protocolNumber;

	@JsonProperty("subject")
	@Column(name = "subject")
	private String subject;

	@JsonProperty("issueDate")
	@Column(name = "issueDate")
	private Long issueDate;

	@JsonProperty("organizationId")
	@Column(name = "organizationId")
	private String organizationId;

	@JsonProperty("signerIds")
	@Column(name = "signerIds")
	@Embedded
	private List<String> signerIds = new ArrayList<>();
	@JsonProperty("unitIds")
	@Column(name = "unitIds")
	@Embedded
	private List<String> unitIds = new ArrayList<>();

	@JsonProperty("decisionTypeId")
	@Column(name = "decisionTypeId")
	private String decisionTypeId;

	@JsonProperty("thematicCategoryIds")
	@Column(name = "thematicCategoryIds")
	@Embedded
	private List<String> thematicCategoryIds = new ArrayList<>();

	@JsonProperty("extraFieldValues")
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "id")
	private ExtraFieldValues extraFieldValues;

	@JsonProperty("privateData")
	@Column(name = "privateData")
	private Boolean privateData;

	@JsonProperty("ada")
	@Column(name = "ada")
	private String ada;

	@JsonProperty("publishTimestamp")
	@Column(name = "publishTimestamp")
	private Long publishTimestamp;

	@JsonProperty("submissionTimestamp")
	@Column(name = "submissionTimestamp")
	private Long submissionTimestamp;

	@JsonProperty("versionId")
	@Column(name = "versionId")
	@Id
	private String versionId;

	@JsonProperty("status")
	@Column(name = "status")
	private String status;

	@JsonProperty("url")
	@Column(name = "url")
	private String url;

	@JsonProperty("documentUrl")
	@Column(name = "documentUrl")
	private String documentUrl;

	@JsonProperty("documentChecksum")
	@Column(name = "documentChecksum")
	private String documentChecksum;

	@JsonProperty("attachments")
	@OneToMany(fetch = FetchType.LAZY)
	private List<Attachment> attachments = new ArrayList<>();

	@JsonProperty("warnings")
	@Column(name = "warnings")
	private String warnings;

	@JsonProperty("correctedVersionId")
	@Column(name = "correctedVersionId")
	private String correctedVersionId;

	@JsonIgnore
	@Embedded
	@Column(name = "additionalProperties")
	private Map<String, String> additionalProperties = new HashMap<String, String>();

	@JsonProperty("protocolNumber")
	public String getProtocolNumber() {
		return protocolNumber;
	}

	@JsonProperty("protocolNumber")
	public void setProtocolNumber(String protocolNumber) {
		this.protocolNumber = protocolNumber;
	}

	@JsonProperty("subject")
	public String getSubject() {
		return subject;
	}

	@JsonProperty("subject")
	public void setSubject(String subject) {
		this.subject = subject;
	}

	@JsonProperty("issueDate")
	public Long getIssueDate() {
		return issueDate;
	}

	@JsonProperty("issueDate")
	public void setIssueDate(Long issueDate) {
		this.issueDate = issueDate;
	}

	@JsonProperty("organizationId")
	public String getOrganizationId() {
		return organizationId;
	}

	@JsonProperty("organizationId")
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	@JsonProperty("signerIds")
	public List<String> getSignerIds() {
		return signerIds;
	}

	@JsonProperty("signerIds")
	public void setSignerIds(List<String> signerIds) {
		this.signerIds = signerIds;
	}

	@JsonProperty("unitIds")
	public List<String> getUnitIds() {
		return unitIds;
	}

	@JsonProperty("unitIds")
	public void setUnitIds(List<String> unitIds) {
		this.unitIds = unitIds;
	}

	@JsonProperty("decisionTypeId")
	public String getDecisionTypeId() {
		return decisionTypeId;
	}

	@JsonProperty("decisionTypeId")
	public void setDecisionTypeId(String decisionTypeId) {
		this.decisionTypeId = decisionTypeId;
	}

	@JsonProperty("thematicCategoryIds")
	public List<String> getThematicCategoryIds() {
		return thematicCategoryIds;
	}

	@JsonProperty("thematicCategoryIds")
	public void setThematicCategoryIds(List<String> thematicCategoryIds) {
		this.thematicCategoryIds = thematicCategoryIds;
	}

	@JsonProperty("extraFieldValues")
	public ExtraFieldValues getExtraFieldValues() {
		return extraFieldValues;
	}

	@JsonProperty("extraFieldValues")
	public void setExtraFieldValues(ExtraFieldValues extraFieldValues) {
		this.extraFieldValues = extraFieldValues;
	}

	@JsonProperty("privateData")
	public Boolean getPrivateData() {
		return privateData;
	}

	@JsonProperty("privateData")
	public void setPrivateData(Boolean privateData) {
		this.privateData = privateData;
	}

	@JsonProperty("ada")
	public String getAda() {
		return ada;
	}

	@JsonProperty("ada")
	public void setAda(String ada) {
		this.ada = ada;
	}

	@JsonProperty("publishTimestamp")
	public Long getPublishTimestamp() {
		return publishTimestamp;
	}

	@JsonProperty("publishTimestamp")
	public void setPublishTimestamp(Long publishTimestamp) {
		this.publishTimestamp = publishTimestamp;
	}

	@JsonProperty("submissionTimestamp")
	public Long getSubmissionTimestamp() {
		return submissionTimestamp;
	}

	@JsonProperty("submissionTimestamp")
	public void setSubmissionTimestamp(Long submissionTimestamp) {
		this.submissionTimestamp = submissionTimestamp;
	}

	@JsonProperty("versionId")
	public String getVersionId() {
		return versionId;
	}

	@JsonProperty("versionId")
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty("url")
	public String getUrl() {
		return url;
	}

	@JsonProperty("url")
	public void setUrl(String url) {
		this.url = url;
	}

	@JsonProperty("documentUrl")
	public String getDocumentUrl() {
		return documentUrl;
	}

	@JsonProperty("documentUrl")
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

	@JsonProperty("documentChecksum")
	public Object getDocumentChecksum() {
		return documentChecksum;
	}

	@JsonProperty("documentChecksum")
	public void setDocumentChecksum(String documentChecksum) {
		this.documentChecksum = documentChecksum;
	}

	@JsonProperty("attachments")
	public List<Attachment> getAttachments() {
		return attachments;
	}

	@JsonProperty("attachments")
	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	@JsonProperty("warnings")
	public Object getWarnings() {
		return warnings;
	}

	@JsonProperty("warnings")
	public void setWarnings(String warnings) {
		this.warnings = warnings;
	}

	@JsonProperty("correctedVersionId")
	public Object getCorrectedVersionId() {
		return correctedVersionId;
	}

	@JsonProperty("correctedVersionId")
	public void setCorrectedVersionId(String correctedVersionId) {
		this.correctedVersionId = correctedVersionId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, String> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, String value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(protocolNumber).append(subject).append(issueDate).append(organizationId)
				.append(signerIds).append(unitIds).append(decisionTypeId).append(thematicCategoryIds)
				// .append(extraFieldValues).append(privateData).append(ada).append(publishTimestamp)
				.append(submissionTimestamp).append(versionId).append(status).append(url).append(documentUrl)
				// .append(documentChecksum).append(attachments).append(warnings).append(correctedVersionId)
				.append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Decision) == false) {
			return false;
		}
		Decision rhs = ((Decision) other);
		return new EqualsBuilder().append(protocolNumber, rhs.protocolNumber).append(subject, rhs.subject)
				.append(issueDate, rhs.issueDate).append(organizationId, rhs.organizationId)
				.append(signerIds, rhs.signerIds).append(unitIds, rhs.unitIds)
				.append(decisionTypeId, rhs.decisionTypeId).append(thematicCategoryIds, rhs.thematicCategoryIds)
				// .append(extraFieldValues,
				// rhs.extraFieldValues).append(privateData, rhs.privateData)
				.append(ada, rhs.ada).append(publishTimestamp, rhs.publishTimestamp)
				.append(submissionTimestamp, rhs.submissionTimestamp).append(versionId, rhs.versionId)
				.append(status, rhs.status).append(url, rhs.url).append(documentUrl, rhs.documentUrl)
				// .append(documentChecksum,
				// rhs.documentChecksum).append(attachments, rhs.attachments)
				.append(warnings, rhs.warnings).append(correctedVersionId, rhs.correctedVersionId)
				.append(additionalProperties, rhs.additionalProperties).isEquals();
	}

}