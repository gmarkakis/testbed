package com.company.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * JPA/Jackson Entity (Org)
 * 
 * @author Georgios Markakis
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "textRelatedADA" })
@Embeddable
public class RelatedAnalipsiYpoxreosi implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("textRelatedADA")
	private String textRelatedADA;

	@JsonIgnore
	@Column(name = "additionalProperties")
	@Embedded
	private Map<String, String> additionalProperties = new HashMap<String, String>();

	@JsonProperty("textRelatedADA")
	public String getTextRelatedADA() {
		return textRelatedADA;
	}

	@JsonProperty("textRelatedADA")
	public void setTextRelatedADA(String textRelatedADA) {
		this.textRelatedADA = textRelatedADA;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, String> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, String value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(textRelatedADA).append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof RelatedAnalipsiYpoxreosi) == false) {
			return false;
		}
		RelatedAnalipsiYpoxreosi rhs = ((RelatedAnalipsiYpoxreosi) other);
		return new EqualsBuilder().append(textRelatedADA, rhs.textRelatedADA)
				.append(additionalProperties, rhs.additionalProperties).isEquals();
	}

}