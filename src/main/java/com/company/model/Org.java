package com.company.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * JPA/Jackson Entity (Org)
 * 
 * @author Georgios Markakis
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "afmType", "noVATOrg" })
@Entity
@IdClass(Org.OrgCompositePK.class)
public class Org implements Serializable {

	public static class OrgCompositePK implements Serializable {

		private static final long serialVersionUID = 1L;

		protected String afmType;
		protected String noVATOrg;

		public OrgCompositePK() {
		}

		public OrgCompositePK(String afmType, String noVATOrg) {
			this.afmType = afmType;
			this.noVATOrg = noVATOrg;
		}
		// equals, hashCode
	}

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@JsonProperty("afmType")
	@Id
	private String afmType;

	@JsonProperty("noVATOrg")
	@Id
	private String noVATOrg;

	@JsonIgnore
	@Embedded
	private Map<String, String> additionalProperties = new HashMap<String, String>();

	@JsonProperty("afmType")
	public String getAfmType() {
		return afmType;
	}

	@JsonProperty("afmType")
	public void setAfmType(String afmType) {
		this.afmType = afmType;
	}

	@JsonProperty("noVATOrg")
	public String getNoVATOrg() {
		return noVATOrg;
	}

	@JsonProperty("noVATOrg")
	public void setNoVATOrg(String noVATOrg) {
		this.noVATOrg = noVATOrg;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, String> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, String value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(afmType).append(noVATOrg).append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Org) == false) {
			return false;
		}
		Org rhs = ((Org) other);
		return new EqualsBuilder().append(afmType, rhs.afmType).append(noVATOrg, rhs.noVATOrg)
				.append(additionalProperties, rhs.additionalProperties).isEquals();
	}

}